package de.ruv.javaschulung.Lektion1Variablen;

import java.lang.reflect.Method;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase {
	/**
	 * Create the test case
	 *
	 * @param testName name of the test case
	 */
	public AppTest(String testName) {
		super(testName);
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		return new TestSuite(AppTest.class);
	}

	/**
	 * Test if class exists
	 */
	public void testAppExists() {
		App app = new App();
	}

	/**
	 * Test if class contains main
	 */
	public void testAppContainsMain() {
		Method[] methods = App.class.getDeclaredMethods();

		boolean found = false;
		for (Method method : methods) {
			if (method.getName().equals("main")) {
				found = true;
			}
		}

		assert (found);
	}

}
