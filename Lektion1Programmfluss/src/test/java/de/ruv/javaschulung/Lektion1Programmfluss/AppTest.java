package de.ruv.javaschulung.Lektion1Programmfluss;

import java.util.ArrayList;
import java.util.Arrays;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase {
	/**
	 * Create the test case
	 *
	 * @param testName name of the test case
	 */
	public AppTest(String testName) {
		super(testName);
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		return new TestSuite(AppTest.class);
	}

	/**
	 * Test sieben
	 */
	public void testSieben() {
		App app = new App();
		assertEquals(7, app.sieben());
		assertEquals(7, app.sieben());
	}

	/**
	 * Test fuenfOderSechs
	 */
	public void testFuenfOderSechs() {
		App app = new App();

		// Kleiner als 10
		assertEquals(5, app.fuenfOderSechs(-3));
		assertEquals(5, app.fuenfOderSechs(0));
		assertEquals(5, app.fuenfOderSechs(7));

		// Größer gleich 10
		assertEquals(6, app.fuenfOderSechs(10));
		assertEquals(6, app.fuenfOderSechs(14));
	}

	/**
	 * Test summeZwei
	 */
	public void testSummeZwei() {
		App app = new App();

		assertEquals(0, app.summeZwei(11));
		assertEquals(0, app.summeZwei(7));
		assertEquals(2, app.summeZwei(2));
		assertEquals(4, app.summeZwei(4));
		assertEquals(2, app.summeZwei(6));
		assertEquals(16, app.summeZwei(256));
	}

	/**
	 * Test summeMindestensZwei
	 */
	public void testSummeMindestensZwei() {
		App app = new App();

		assertEquals(2, app.summeMindestensZwei(11));
		assertEquals(2, app.summeMindestensZwei(7));
		assertEquals(2, app.summeMindestensZwei(2));
		assertEquals(4, app.summeMindestensZwei(4));
		assertEquals(2, app.summeMindestensZwei(6));
		assertEquals(16, app.summeMindestensZwei(256));
	}

	/**
	 * test summeGauss
	 */
	public void testSummeGauss() {
		App app = new App();

		int n = 5;
		assertEquals(n * (n + 1) / 2, app.summeGauss(n));

		n = 1;
		assertEquals(n * (n + 1) / 2, app.summeGauss(n));

		n = 128;
		assertEquals(n * (n + 1) / 2, app.summeGauss(n));
	}

	/**
	 * test summeArray
	 */
	public void testSummeArray() {
		App app = new App();

		int[] n = new int[] { 5, 6, 7 };
		assertEquals(Arrays.stream(n).reduce(0, (a, b) -> a + b), app.summeArray(n));

		n = new int[] { 1, 2, 3, 4 };
		assertEquals(Arrays.stream(n).reduce(0, (a, b) -> a + b), app.summeArray(n));

		n = new int[] { 128, 64, 1024 };
		assertEquals(Arrays.stream(n).reduce(0, (a, b) -> a + b), app.summeArray(n));

		n = new int[] { 2, 3, 5, 7, 11, 19 };
		assertEquals(Arrays.stream(n).reduce(0, (a, b) -> a + b), app.summeArray(n));
	}

	public void testEinsDreiOderFuenf() {
		App app = new App();

		assertEquals(1, app.einsDreiOderFuenf('A'));
		assertEquals(5, app.einsDreiOderFuenf('B'));
		assertEquals(3, app.einsDreiOderFuenf('C'));
		assertEquals(0, app.einsDreiOderFuenf('Q'));
		assertEquals(0, app.einsDreiOderFuenf('F'));
	}
}
