package de.ruv.javaschulung.Lektion1Programmfluss;

/**
 * Lektion 1 Programmfluss
 *
 */
public class App {
	/**
	 * Diese Methode soll sieben zurückgeben.
	 * 
	 * @return sieben
	 */
	public int sieben() {
		// VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV
		// Hier rein!

		// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
		return zahl;
	}

	/**
	 * Diese Methode soll fünf zurückgeben, wenn `eingabe` kleiner ist als zehn
	 * ansonsten sechs.
	 * 
	 * @param eingabe Eingabe die zu prüfen ist
	 * @return fünf oder sechs
	 */
	public int fuenfOderSechs(int eingabe) {
		// VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV
		// Hier rein!

		// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
		return zahl;
	}

	/**
	 * Die Methode soll die Zahl zwei so lange addieren, solange `eingabe` durch
	 * zwei teilbar ist. Startwert: 0
	 * 
	 * @param eingabe Eingabe die zu prüfen ist
	 * @return Summe aus zwei
	 */
	public int summeZwei(int eingabe) {
		// VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV
		// Hier rein!

		// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
		return zahl;
	}

	/**
	 * Die Methode soll die Zahl zwei so lange addieren, solange `eingabe` durch
	 * zwei teilbar ist. Dies soll aber mindestens einmal geschehen. Startwert: 0
	 * 
	 * @param eingabe Eingabe die zu prüfen ist
	 * @return Summe aus zwei
	 */
	public int summeMindestensZwei(int eingabe) {
		// VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV
		// Hier rein!

		// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
		return zahl;
	}

	/**
	 * Die Methode soll jede Zahl von 1 bis `eingabe` addieren.
	 * 
	 * @param eingabe Die Eingabe, bis wie oft addiert werden soll
	 * @return Summe aller Zahlen von 1 bis `eingabe`
	 */
	public int summeGauss(int eingabe) {
		// VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV
		// Hier rein!

		// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
		return zahl;
	}

	/**
	 * Die Methode soll alle Zahlen des Arrays addieren.
	 * 
	 * @param eingabe Array der Zahlen, die zu addieren sind
	 * @return Summe der Zahlen des Arrays
	 */
	public int summeArray(int[] eingabe) {
		// VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV
		// Hier rein!

		// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
		return zahl;
	}

	/**
	 * Die Methode soll eins züruckgeben, wenn `eingabe` 'A' entspricht, fünf wenn
	 * `eingabe` 'B' ist und drei bei `eingabe` gleich 'C'. Ansonsten null.
	 * 
	 * @param eingabe Array der Zahlen, die zu addieren sind
	 * @return Summe der Zahlen des Arrays
	 */
	public int einsDreiOderFuenf(char eingabe) {
		// VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV
		// Hier rein!

		// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
		return zahl;
	}

	public static void main(String[] args) {
		App app = new App();

		System.out.println("sieben(): " + app.sieben());

		System.out.println("fuenfOderSechs(2): " + app.fuenfOderSechs(2));
		System.out.println("fuenfOderSechs(15): " + app.fuenfOderSechs(15));

		System.out.println("summeZwei(256): " + app.summeZwei(256));
		System.out.println("summeZwei(255): " + app.summeZwei(255));

		System.out.println("summeMindestensZwei(256): " + app.summeMindestensZwei(256));
		System.out.println("summeMindestensZwei(255): " + app.summeMindestensZwei(255));

		System.out.println("summeGauss(10): " + app.summeGauss(10));
		System.out.println("summeGauss(100): " + app.summeGauss(100));

		System.out.println("summeArray(new int[]{1, 2, 3, 4}): " + app.summeArray(new int[] { 1, 2, 3, 4 }));
		System.out.println("summeArray(new int[]{4, 2, 7, 5}): " + app.summeArray(new int[] { 4, 2, 7, 5 }));

		System.out.println("einsDreiOderFuenf('A'}): " + app.einsDreiOderFuenf('A'));
		System.out.println("einsDreiOderFuenf('B'}): " + app.einsDreiOderFuenf('B'));
		System.out.println("einsDreiOderFuenf('C'}): " + app.einsDreiOderFuenf('C'));

	}
}
